package Lesson3.Velychko.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage extends Page {
	
	@FindBy(id = "subtab-AdminCatalog")
	WebElement adminCatalog;
	
	@FindBy(xpath = "//li[@id='subtab-AdminCategories']/a")
	WebElement adminCategories;
	
	public DashboardPage(WebDriver webDriver) {
		super(webDriver);
		PageFactory.initElements(super.driver, this);
	}
	
	public void openAdminCategories() {
		super.onElementHower(adminCatalog);
		super.waitAndClickElement(this.adminCategories);
	}
}
