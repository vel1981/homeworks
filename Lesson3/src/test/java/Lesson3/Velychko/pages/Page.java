package Lesson3.Velychko.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;

public abstract class Page {

  protected WebDriver driver;
  private Actions action;
  private WebDriverWait wait; 

  public Page(WebDriver driver) {
    this.driver = driver;
    this.action = new Actions(driver);
  }

  public void open(String urlPage) {
	this.driver.get(urlPage);
  }
  
  public String getTitle() {
    return driver.getTitle();
  }

  public WebDriverWait getDriverWait() {
	if (this.wait == null) {
		this.wait = new WebDriverWait(driver, 15000, 1000);
	}
	return wait;	 
  }
  
  public void waitElementDisplay(WebElement webElement) {
	try {
		this.getDriverWait().until(ExpectedConditions.visibilityOf(webElement));
	} catch (org.openqa.selenium.TimeoutException | org.openqa.selenium.NoSuchElementException te) {
		AssertionError assertionError = new AssertionError(te);
		throw assertionError;
	}				
  }

  public boolean waitElementClickable(WebElement webElement) {
	try {
		this.getDriverWait().until(ExpectedConditions.elementToBeClickable(webElement));
		return true;
	} catch (org.openqa.selenium.TimeoutException te){						
		return false;
	}		
  }
  
  public boolean waitElementLocated(By locator) {
	try {	  
	  this.getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(locator));
	  return true;
	} catch (org.openqa.selenium.TimeoutException te){						
		return false;
	}	  
  }
  
  protected boolean waitAndClickElement(WebElement webElement) {		
	try {
		if (this.waitElementClickable(webElement)) {
			webElement.click();
			return true;
		} else
			return false;
	} catch (NoSuchElementException nsee) {
		return false;
	}
  }		
  
  public boolean waitTitle(String title) {
	try {
		getDriverWait().until(ExpectedConditions.titleContains(title));
		return true;
	} catch (org.openqa.selenium.TimeoutException te) {
		return false;
	}			
  }
  
  public void type(WebElement webElement, String keys) {
	waitElementDisplay(webElement);
	if (webElement.isEnabled()) {
		webElement.clear();
		webElement.sendKeys(keys);
	}
  }
  
  public void type_js(WebElement webElement, String keys) {
	  String script = "var elem = arguments[0]; var value = arguments[1]; elem.value = value;";
	  ((JavascriptExecutor)driver).executeScript(script, webElement, keys);
  }
  
  public void onElementHower(WebElement webElement) {
	  waitElementDisplay(webElement);
	  this.action.moveToElement(webElement).build().perform();
  }
  
}
