package Lesson3.Velychko.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends Page {
	
	@FindBy(id = "email")
	WebElement email;

	@FindBy(id = "passwd")
	WebElement passwd;
	
	@FindBy(xpath = "//span[@class='ladda-label']/parent::button")
	WebElement submitLogin;

	public LoginPage(WebDriver webDriver) {
		super(webDriver);
		PageFactory.initElements(super.driver, this);
	}

	public void fillEmail(String emailString) {
		super.type(this.email, emailString);
	}
	
	public void fillPasswd(String passwdString) {
		super.type(this.passwd, passwdString);
	}
	
	public void enterLogin() {
		super.waitAndClickElement(this.submitLogin);
	}
}
