package Lesson3.Velychko;

import Lesson3.Velychko.pages.LoginPage;
import Lesson3.Velychko.pages.DashboardPage;
import Lesson3.Velychko.utils.BrowserDriver;


public class lesson3 {

	public static void main(String[] args) {
		LoginPage loginPage = new LoginPage(BrowserDriver.getDriver());
		DashboardPage dashboardPage = new DashboardPage(BrowserDriver.getDriver());
		
		loginPage.open("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
		loginPage.waitTitle("Панель администратора");
		loginPage.fillEmail("webinar.test@gmail.com");
		loginPage.fillPasswd("Xcg7299bnSmMuRLp9ITw");
		loginPage.enterLogin();
		
		dashboardPage.openAdminCategories();
		
		BrowserDriver.closeDriver();
	}

}
